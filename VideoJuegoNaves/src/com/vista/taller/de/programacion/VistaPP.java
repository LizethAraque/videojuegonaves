/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vista.taller.de.programacion;

import com.thread.taller.de.programacion.Nave;
import com.thread.taller.de.programacion.NavesEnem;
import com.thread.taller.de.programacion.SonidoLogin;

/**
 *
 * @author lizeth
 */
public class VistaPP extends javax.swing.JFrame {

    //instanciando hilo de sonido
    SonidoLogin sonido = new SonidoLogin();
    Thread threadSonido = new Thread(sonido);

    //instanciando hilo de naves Enemigas
    NavesEnem navesE;
    Thread threadNavesE;

    //instanciando hilo de naves Enemigas
    Nave nave;

    public VistaPP() {
        initComponents();
        setPropiedades();
    }

    private void setPropiedades() {
        laser.setVisible(false);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        threadSonido.start();
        this.jp_Naves.setSize(this.getBounds().getSize().width, this.getBounds().getSize().height);
        nave = new Nave(this.jp_Naves.getBounds().getMinY(), this.jp_Naves.getBounds().getMaxX(), this.jp_Naves.getBounds().getMinX());
        navesE = new NavesEnem(this.jp_Naves.getBounds().getMaxX(), this.jp_Naves.getBounds().getMinX(), this.jp_Naves.getBounds().getMaxY(), this.jp_Naves.getBounds().getMinY());
        threadNavesE = new Thread(navesE);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jp_Login = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        playBtn = new javax.swing.JButton();
        fondo = new javax.swing.JLabel();
        jp_Naves = new javax.swing.JPanel();
        navePrincipal = new javax.swing.JLabel();
        nave1 = new javax.swing.JLabel();
        nave2 = new javax.swing.JLabel();
        nave3 = new javax.swing.JLabel();
        nave5 = new javax.swing.JLabel();
        nave6 = new javax.swing.JLabel();
        nave4 = new javax.swing.JLabel();
        nave7 = new javax.swing.JLabel();
        nave8 = new javax.swing.JLabel();
        nave9 = new javax.swing.JLabel();
        laser = new javax.swing.JLabel();
        fondo1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(700, 651));

        jp_Login.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/taller/de/programacion/log2.png"))); // NOI18N
        jLabel1.setText("jLabel1");
        jp_Login.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 40, 550, 170));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("BY LIZETH GIOVANNA ARAQUE ");
        jp_Login.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 550, 420, 50));

        playBtn.setBackground(new java.awt.Color(153, 153, 153));
        playBtn.setForeground(new java.awt.Color(255, 255, 255));
        playBtn.setText("PLAY");
        playBtn.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        playBtn.setFocusPainted(false);
        playBtn.setFocusable(false);
        playBtn.setOpaque(false);
        playBtn.setSelected(true);
        playBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                playBtnActionPerformed(evt);
            }
        });
        jp_Login.add(playBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 280, 400, 50));

        fondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/taller/de/programacion/fond3.jpg"))); // NOI18N
        fondo.setText("jLabel1");
        jp_Login.add(fondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 700, 670));

        jp_Naves.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        navePrincipal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/taller/de/programacion/N1F.png"))); // NOI18N
        jp_Naves.add(navePrincipal, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 470, 110, 110));

        nave1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/taller/de/programacion/na1.png"))); // NOI18N
        jp_Naves.add(nave1, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 30, -1, 50));

        nave2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/taller/de/programacion/na1.png"))); // NOI18N
        jp_Naves.add(nave2, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 30, -1, 50));

        nave3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/taller/de/programacion/na1.png"))); // NOI18N
        jp_Naves.add(nave3, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 30, -1, 50));

        nave5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/taller/de/programacion/na1.png"))); // NOI18N
        jp_Naves.add(nave5, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 130, -1, 50));

        nave6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/taller/de/programacion/na1.png"))); // NOI18N
        jp_Naves.add(nave6, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 30, -1, 50));

        nave4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/taller/de/programacion/na1.png"))); // NOI18N
        jp_Naves.add(nave4, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 80, -1, 50));

        nave7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/taller/de/programacion/na1.png"))); // NOI18N
        jp_Naves.add(nave7, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 80, -1, 50));

        nave8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/taller/de/programacion/na1.png"))); // NOI18N
        jp_Naves.add(nave8, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 80, -1, 50));

        nave9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/taller/de/programacion/na1.png"))); // NOI18N
        jp_Naves.add(nave9, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 30, -1, 50));

        laser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/taller/de/programacion/laser.png"))); // NOI18N
        laser.setText("jLabel3");
        jp_Naves.add(laser, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 510, 10, 60));

        fondo1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/taller/de/programacion/fond3.jpg"))); // NOI18N
        fondo1.setText("jLabel1");
        jp_Naves.add(fondo1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 710, 670));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jp_Login, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jp_Naves, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jp_Login, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(9, 9, 9)
                    .addComponent(jp_Naves, javax.swing.GroupLayout.PREFERRED_SIZE, 651, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void playBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_playBtnActionPerformed

        jp_Naves.setVisible(true);
        jp_Login.setVisible(false);
        sonido.getSound().stop();
        threadNavesE.start();
        this.addKeyListener(nave);
    }//GEN-LAST:event_playBtnActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VistaPP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VistaPP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VistaPP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VistaPP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VistaPP().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel fondo;
    private javax.swing.JLabel fondo1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jp_Login;
    private javax.swing.JPanel jp_Naves;
    public static javax.swing.JLabel laser;
    public static javax.swing.JLabel nave1;
    public static javax.swing.JLabel nave2;
    public static javax.swing.JLabel nave3;
    public static javax.swing.JLabel nave4;
    public static javax.swing.JLabel nave5;
    public static javax.swing.JLabel nave6;
    public static javax.swing.JLabel nave7;
    public static javax.swing.JLabel nave8;
    public static javax.swing.JLabel nave9;
    public static javax.swing.JLabel navePrincipal;
    private javax.swing.JButton playBtn;
    // End of variables declaration//GEN-END:variables
}
