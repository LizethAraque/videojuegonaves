package com.thread.taller.de.programacion;

import com.vista.taller.de.programacion.VistaPP;
import java.util.Date;

public class NavesEnem implements Runnable {

    private final double posMaxX;
    private final double posMinX;
    private final double posMaxY;
    private final double posMinY;
    private String restar = "+";
    private Date time;
    private boolean state;
    
    public NavesEnem(double posMaxX, double posMinX, double posMaxY, double posMinY) {
        this.posMaxX = posMaxX - 70;
        this.posMinX = posMinX;
        this.posMaxY = posMaxY - 250;
        this.posMinY = posMinY;
    }

    @Override
    public void run() {
        try {
            long miles;
            time = new Date();
            while (!state) {
                miles = new Date().getTime() - time.getTime();
                if (miles >= 5000) {
                    time = new Date();
                    moverPositionY();
                }
                moverNaveSeis();
                moverNaveNueve();
                moverNaveTres();
                moverNaveDos();
                moverNaveUno();
                naveOcho();
                naveSiete();
                naveCuatro();
                moverNaveCinco();
                checkLaser();
                checkPlayer();
                Thread.sleep(1000);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkLaser() {
        if (VistaPP.nave2.getBounds().intersects(VistaPP.laser.getBounds())) {
            VistaPP.nave2.setVisible(false);
        }

        if (VistaPP.nave3.getBounds().intersects(VistaPP.laser.getBounds())) {
            VistaPP.nave3.setVisible(false);
        }

        if (VistaPP.nave9.getBounds().intersects(VistaPP.laser.getBounds())) {
            VistaPP.nave9.setVisible(false);
        }

        if (VistaPP.nave4.getBounds().intersects(VistaPP.laser.getBounds())) {
            VistaPP.nave4.setVisible(false);
        }

        if (VistaPP.nave7.getBounds().intersects(VistaPP.laser.getBounds())) {
            VistaPP.nave7.setVisible(false);
        }

        if (VistaPP.nave8.getBounds().intersects(VistaPP.laser.getBounds())) {
            VistaPP.nave8.setVisible(false);
        }

        if (VistaPP.nave5.getBounds().intersects(VistaPP.laser.getBounds())) {
            VistaPP.nave5.setVisible(false);
        }

        if (VistaPP.nave6.getBounds().intersects(VistaPP.laser.getBounds())) {
            VistaPP.nave6.setVisible(false);
        }

        if (VistaPP.nave1.getBounds().intersects(VistaPP.laser.getBounds())) {
            VistaPP.nave1.setVisible(false);
        }
    }

    private void checkPlayer() {
        if (VistaPP.nave1.isVisible() && VistaPP.nave1.getLocation().y >= posMaxY || VistaPP.nave2.isVisible() && VistaPP.nave2.getLocation().y >= posMaxY || VistaPP.nave3.isVisible() && VistaPP.nave3.getLocation().y >= posMaxY || VistaPP.nave9.isVisible() && VistaPP.nave9.getLocation().y >= posMaxY
                || VistaPP.nave4.isVisible() && VistaPP.nave4.getLocation().y >= posMaxY || VistaPP.nave7.isVisible() && VistaPP.nave7.getLocation().y >= posMaxY || VistaPP.nave8.isVisible() && VistaPP.nave8.getLocation().y >= posMaxY
                || VistaPP.nave5.isVisible() && VistaPP.nave5.getLocation().y >= posMaxY || VistaPP.nave6.isVisible() && VistaPP.nave6.getLocation().y >= posMaxY) {

            state = true;
        }

        if (!VistaPP.nave1.isVisible() && !VistaPP.nave2.isVisible() && !VistaPP.nave3.isVisible() && !VistaPP.nave9.isVisible()
                && !VistaPP.nave4.isVisible() && !VistaPP.nave7.isVisible() && !VistaPP.nave8.isVisible() && !VistaPP.nave5.isVisible() && !VistaPP.nave6.isVisible()) {
            state = true;
        }
    }

    private void moverPositionY() {
        VistaPP.nave1.setLocation(VistaPP.nave1.getLocation().x, VistaPP.nave2.getLocation().y + 30);
        VistaPP.nave2.setLocation(VistaPP.nave2.getLocation().x, VistaPP.nave2.getLocation().y + 30);
        VistaPP.nave3.setLocation(VistaPP.nave3.getLocation().x, VistaPP.nave3.getLocation().y + 30);
        VistaPP.nave9.setLocation(VistaPP.nave9.getLocation().x, VistaPP.nave9.getLocation().y + 30);
        VistaPP.nave4.setLocation(VistaPP.nave4.getLocation().x, VistaPP.nave4.getLocation().y + 30);
        VistaPP.nave7.setLocation(VistaPP.nave7.getLocation().x, VistaPP.nave7.getLocation().y + 30);
        VistaPP.nave8.setLocation(VistaPP.nave8.getLocation().x, VistaPP.nave8.getLocation().y + 30);
        VistaPP.nave5.setLocation(VistaPP.nave5.getLocation().x, VistaPP.nave5.getLocation().y + 30);
        VistaPP.nave6.setLocation(VistaPP.nave6.getLocation().x, VistaPP.nave6.getLocation().y + 30);
    }

    private void moverNaveCinco() {
        if (VistaPP.nave6.getLocation().x <= posMaxX && restar.contains("+")) {
            VistaPP.nave5.setLocation(VistaPP.nave5.getLocation().x + 30, VistaPP.nave5.getLocation().y);
        } else if (VistaPP.nave1.getLocation().x >= posMinX) {
            VistaPP.nave5.setLocation(VistaPP.nave5.getLocation().x - 30, VistaPP.nave5.getLocation().y);
            restar = "-";
        } else {
            restar = "+";
        }
    }

    private void moverNaveUno() {
        if (VistaPP.nave6.getLocation().x <= posMaxX && restar.contains("+")) {
            VistaPP.nave1.setLocation(VistaPP.nave1.getLocation().x + 30, VistaPP.nave1.getLocation().y);
        } else if (VistaPP.nave1.getLocation().x >= posMinX) {
            VistaPP.nave1.setLocation(VistaPP.nave1.getLocation().x - 30, VistaPP.nave1.getLocation().y);
            restar = "-";
        } else {
            restar = "+";
        }
    }

    private void moverNaveDos() {
        if (VistaPP.nave6.getLocation().x <= posMaxX && restar.contains("+")) {
            VistaPP.nave2.setLocation(VistaPP.nave2.getLocation().x + 30, VistaPP.nave2.getLocation().y);
        } else if (VistaPP.nave1.getLocation().x >= posMinX) {
            VistaPP.nave2.setLocation(VistaPP.nave2.getLocation().x - 30, VistaPP.nave2.getLocation().y);
            restar = "-";
        } else {
            restar = "+";
        }
    }

    private void moverNaveTres() {
        if (VistaPP.nave6.getLocation().x <= posMaxX && restar.contains("+")) {
            VistaPP.nave3.setLocation(VistaPP.nave3.getLocation().x + 30, VistaPP.nave3.getLocation().y);
        } else if (VistaPP.nave1.getLocation().x >= posMinX) {
            VistaPP.nave3.setLocation(VistaPP.nave3.getLocation().x - 30, VistaPP.nave3.getLocation().y);
            restar = "-";
        } else {
            restar = "+";
        }
    }

    private void moverNaveNueve() {
        if (VistaPP.nave6.getLocation().x <= posMaxX && restar.contains("+")) {
            VistaPP.nave9.setLocation(VistaPP.nave9.getLocation().x + 30, VistaPP.nave9.getLocation().y);
        } else if (VistaPP.nave1.getLocation().x >= posMinX) {
            VistaPP.nave9.setLocation(VistaPP.nave9.getLocation().x - 30, VistaPP.nave9.getLocation().y);
            restar = "-";
        } else {
            restar = "+";
        }
    }

    private void naveSiete() {
        if (VistaPP.nave6.getLocation().x <= posMaxX && restar.contains("+")) {
            VistaPP.nave7.setLocation(VistaPP.nave7.getLocation().x + 10, VistaPP.nave7.getLocation().y);
        } else if (VistaPP.nave1.getLocation().x >= posMinX) {
            VistaPP.nave7.setLocation(VistaPP.nave7.getLocation().x - 15, VistaPP.nave7.getLocation().y);
            restar = "-";
        } else {
            restar = "+";
        }
    }

    private void naveCuatro() {
        if (VistaPP.nave6.getLocation().x <= posMaxX && restar.contains("+")) {
            VistaPP.nave4.setLocation(VistaPP.nave4.getLocation().x + 10, VistaPP.nave4.getLocation().y);
        } else if (VistaPP.nave1.getLocation().x >= posMinX) {
            VistaPP.nave4.setLocation(VistaPP.nave4.getLocation().x - 15, VistaPP.nave4.getLocation().y);
            restar = "-";
        } else {
            restar = "+";
        }
    }

    private void naveOcho() {
        if (VistaPP.nave6.getLocation().x <= posMaxX && restar.contains("+")) {
            VistaPP.nave8.setLocation(VistaPP.nave8.getLocation().x + 15, VistaPP.nave8.getLocation().y);
        } else if (VistaPP.nave1.getLocation().x >= posMinX) {
            VistaPP.nave8.setLocation(VistaPP.nave8.getLocation().x - 12, VistaPP.nave8.getLocation().y);
            restar = "-";
        } else {
            restar = "+";
        }
    }

    private void moverNaveSeis() {
        if (VistaPP.nave6.getLocation().x <= posMaxX && restar.contains("+")) {
            VistaPP.nave6.setLocation(VistaPP.nave6.getLocation().x + 30, VistaPP.nave6.getLocation().y);
        } else if (VistaPP.nave1.getLocation().x >= posMinX) {
            VistaPP.nave6.setLocation(VistaPP.nave6.getLocation().x - 30, VistaPP.nave6.getLocation().y);
            restar = "-";
        } else {
            restar = "+";
        }
    }

}
