package com.thread.taller.de.programacion;

import java.io.File;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;

public class SonidoLogin implements Runnable {

    private Clip sound;

    public Clip getSound() {
        return sound;
    }

    public void setSound(Clip sound) {
        this.sound = sound;
    }

    @Override
    public void run() {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("SonidoP.WAV"));
            AudioFormat format = audioInputStream.getFormat();
            DataLine.Info info = new DataLine.Info(Clip.class, format);
            sound = (Clip) AudioSystem.getLine(info);
            sound.open(audioInputStream);
            sound.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
