package com.thread.taller.de.programacion;

import com.vista.taller.de.programacion.VistaPP;

public class Bala implements Runnable {

    private final double posMaxY;
    private final int posicionLaserX;

    public Bala(double posMaxY, int posicionX, int posicionY) {
        this.posMaxY = posMaxY;
        this.posicionLaserX = posicionX;
    }

    @Override
    public void run() {
        while (posMaxY <= VistaPP.laser.getLocation().y) {
            try {
                VistaPP.laser.setLocation(posicionLaserX, VistaPP.laser.getLocation().y - 50);
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        VistaPP.laser.setLocation(posicionLaserX, VistaPP.laser.getLocation().y - 100);
    }
}
