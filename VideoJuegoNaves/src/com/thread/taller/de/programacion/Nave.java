package com.thread.taller.de.programacion;

import com.vista.taller.de.programacion.VistaPP;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Nave implements KeyListener {

    private final double posMaxY;
    private int key;
    private Thread thread = new Thread();
    private Bala laser;
    private final double posMaxX;
    private final double posMinX;

    public Nave(double posMaxY, double posMaxX, double posMinX) {
        this.posMaxY = posMaxY;
        this.posMaxX = posMaxX - 115;
        this.posMinX = posMinX;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        key = e.getKeyCode();
        if (key == KeyEvent.VK_LEFT && posMinX <= VistaPP.navePrincipal.getLocation().x) {
            VistaPP.navePrincipal.setLocation(VistaPP.navePrincipal.getLocation().x - 10, VistaPP.navePrincipal.getLocation().y);
        }
        if (key == KeyEvent.VK_RIGHT && posMaxX >= VistaPP.navePrincipal.getLocation().x) {
            VistaPP.navePrincipal.setLocation(VistaPP.navePrincipal.getLocation().x + 10, VistaPP.navePrincipal.getLocation().y);
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        key = e.getKeyCode();
        if (key == KeyEvent.VK_SPACE) {
            if (!thread.isAlive()) {
                VistaPP.laser.setLocation(VistaPP.navePrincipal.getLocation().x + 35, VistaPP.navePrincipal.getLocation().y);
                VistaPP.laser.setVisible(true);
                laser = new Bala(posMaxY, VistaPP.navePrincipal.getLocation().x + 35, VistaPP.navePrincipal.getLocation().y);
                thread = new Thread(laser);
                thread.start();
            } else {
                System.out.println("esta corriendo laser");
            }
        }
    }
}
